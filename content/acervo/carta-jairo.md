---
title: Carta Jairo Saw – liderança do Movimento Munduruku Ipereg Ayu
date: '2019-02-11T19:27:37.000+10:00'
tags:
- Ipereg Ayu Munduruku
- Munduruku
- Carta
capa:
  descricao: lololol de imagem
  imagem: imagens/img_9181.JPG
descricao: Somos povos nativos da floresta Amazônica, existimos desde a origem da criação do mundo, quando o Karosakaybu nos transformou do barro (argila) e nos soprou com a brisa do seu vento, dando a vida para todos nós.
galeria:
- imagens/dsc_0268.JPG
- imagens/dsc_0768.JPG
- imagens/dsc_4436.JPG
links_relacionados:
- link: https://nao-aogoogle.com
  titulo: meu link
pdfs:
- titulo: Meu pdf
  autoria: Batata
  ano: 2020
  arquivo: imagens/onion-guide-fanzine.pdf
audios:
- titulo: Idade Media
  arquivo: "[archiveorg idademedia width=640 height=140 frameborder=0 webkitallowfullscreen=true
    mozallowfullscreen=true]"

---
Somos povos nativos da floresta Amazônica, existimos desde a origem da criação do mundo, quando o Karosakaybu nos transformou do barro (argila) e nos soprou com a brisa do seu vento, dando a vida para todos nós. Desde o princípio conhecemos o mundo que está ao nosso redor e sabemos da existência do pariwat (não-índio), que já vivia em nosso meio. Éramos um só povo, criado por Karosakaybu, criador e transformador de todos os seres vivos na face da Terra: os animais, as florestas, os rios e a humanidade. Antes, outros povos não existiam, assim como os pariwat não existiam.

O pariwat foi expulso do coração da Amazônia, devido ao seu pensamento muito ambicioso, que só enxergava a grande riqueza material. Portanto, a sua cobiça, a sua ganância, a sua ambição, o seu olho grande despertou o grande interesse econômico sobre o patrimônio que estava em seu poder. Não pretendia proteger, guardar, preservar, manter intactos os bens comuns, o maior patrimônio da humanidade, e isso despertou o seu plano de destruição da vida na Terra. Por isso, o Karosakaybu achou melhor tirar a presença do pariwat deste lugar tão maravilhoso, onde há sombra e água fresca.

Nossos ancestrais, no decorrer do tempo, nos transmitiram oralmente esses relatos sobre a vinda dos pariwat, oriundos de outro continente, a Europa. Contaram-nos que um dia chegariam a esse paraíso onde nós estamos. Hoje podemos presenciar os fatos sendo consumados.

O pariwat chegou, depois de viajar pelo mundo em busca de especiarias, produtos, mercadorias. Foram ampliando a expedição, em busca de conhecer outro mundo ou outra terra. Viajavam em caravelas até chegar ao chamado “novo continente”, que se conhece hoje como continente americano, onde está o Brasil, desde o século XIV.

Nossos avós diziam que, quando os pariwat chegassem até o nosso território, eles iriam tomar nossas terras, nossas mulheres, nossas crianças. Iriam nos matar, não nos poupariam vidas para possuir tudo aquilo que nos pertence: a nossa riqueza, os bens que possuímos, incluindo a nossa cultura, a forma como vivemos.

Invadiram nossa terra, muitos de nossos parentes foram massacrados, assassinados, foram submetidos à tortura e foram usados nos trabalhos forçados, servindo de mão de obra escrava.

Já no século XXI, na era contemporânea/continuamos sendo oprimidos, como nos tempos passados. Apesar de termos alcançado várias conquistas e garantido nossos direitos específicos e diferenciados na Constituição Federa” ainda assim esses direitos não são respeitados e reconhecidos. Hoje se utilizam do poder para impor o lema do “progresso e desenvolvimento”, a base da bandeira nacional: “ordem e progresso”. Tudo em nome do capital.

No primeiro momento, o objetivo era seguir exatamente como está escrito no símbolo da bandeira: pôr em ordem, organizar a política da sociedade civil. As leis estão organizadas desde o princípio, elas não devem ser mudadas, o que se deve fazer é cumprir e obedecer.

Nós, Munduruku, obedecemos leis e, embora não se encontrem escritas em nenhum arquivo, as conhecemos há milhões de anos e até hoje cumprimos essas leis.

A natureza tem leis e devem ser obedecidas. Se nós violarmos suas regras, ela se vingará e sofreremos as conseqüências. As leis estão em ordem, não devem sofrer interferência alguma.

Os “civilizados” escreveram leis e, a despeito delas, usam o poder para oprimir as pessoas que julgam ter menos conhecimentos. Não reconhecem os seus direitos, chegam até a intimidar, a ponto de ficarem submissas. A razão é dada apenas por um individuo ou classe com maior poder econômico.

Os “civilizados” dariam bom exemplo de cidadão pleno e letrado para as pessoas humildes, porque a lei foi feita por causa das injustiças criadas pelos pariwat de outro continente. Justiça é saber o que é certo e o que é errado, sem favorecer a um ou a outro, a balança não deve pesar nem para a direita e nem para a esquerda.

Existe uma haste entre os dois pratos da balança e a justiça deve ser feita para o cumprimento da lei, deve ser obedecida e aplicada a quem tentar infringi-Ia. Então, ao surgir a lei escrita, ela desvendou os nossos olhos, passamos a enxergar as coisas erradas dos pariwat a nosso respeito. Os nossos direitos estão em jogo. Falam tanto a nosso respeito, somos tratados como empecilhos para o desenvolvimento econômico do país. Mas nós não somos contra o desenvolvimento, o que queremos é que sejamos respeitados e que nossos direitos como indígenas sejam reconhecidos. A Constituição diz que é dever do Estado proteger, demarcar os territórios, garantir a segurança, respeitar as formas próprias de organização social e as culturas diferenciadas, por isso queremos respeito. Até a nossa crença, a nossa religião deve levar em consideração o modo como vivemos.

Respeitamos sempre a natureza, ela é de suma importância para nós e é essencial para a vida no planeta. Nós estamos preocupados com o equilíbrio do clima, com as mudanças climáticas. Resta apenas uma parte da floresta que está dando vida ao planeta chamado Terra e a seus habitantes. Esta pequena parte tornou-se alvo da ganância do pariwat.

Nós percebemos que os países ricos queriam levar o chamado “desenvolvimento” para o coração da Amazônia. Não levam em consideração os povos nativos desse continente, que estão aqui há milhares de anos. Estamos lutando, resistindo, protegendo com unhas e dentes esse nosso patrimônio, mas ninguém ouve nossos gritos de socorro em prol da vida no planeta. Sabemos que a vida dos pariwat também está em risco e não estamos apenas nos defendendo: estamos defendendo

toda a vida, toda a biodiversidade.

Existem tantos cientistas que estudam os fenômenos da natureza e alguns devem estar percebendo as mudanças climáticas, dia após dia, ano após ano. Em outros países vemos as conseqüências dos impactos causados pela ação humana. As conseqüências estão sendo sentidas e estão fora da normalidade. A natureza está sofrendo alterações no seu funcionamento, que vão além da sua capacidade, ela já não está suportando a pressão causada pelos humanos.

Alguns exemplos dessa pressão são: poluição do ar produzida pelas grandes fábricas e indústrias, automóveis, desmatamento, explosão de dinamites, dentre outros. A natureza não consegue transformar o oxigênio para devolver para nós, porque a impureza do ar contaminado é maior do que a sua capacidade. O acúmulo de ar poluído torna-se pesado para as árvores. É notado isso claramente nas leis da física.

As árvores não conseguem absorver todo esse ar impuro. O peso do ar não é visto por nós, mas percebemos através do aquecimento. Em algumas regiões, o clima é seco e quente, geralmente as fontes de água secam, secam as relvas, assim como as folhas das árvores caem e os animais não conseguem encontrar abrigos e alimentos. Por falta de vegetação, o equilíbrio está ameaçado, colocando em risco a vida dos homens e dos animais. Não há mais vapores de água produzidos pelas árvores, pela manhã não há gotas de orvalho. Nas grandes cidades, o clima não é diferente. Para dizer a verdade.ias pessoas estão sedentas, cansadas, querem sentir a brisa de ar frio pela manhã. No interior das casas, seja de noite ou de dia, o ambiente não é favorável, já é quente.

Outro fator de alto risco é o acúmulo de gás poluente, as fumaças das grandes queimadas, que chegam e se alojam na camada de ozônio. Muitas vezes chegam pouco a pouco de algumas regiões e outras vezes chegam em grandes quantidades, aumentando a extensão do volume de gás poluente, rompendo a barreira de proteção da filtração de raios solares em direção à terra. Nem podemos imaginar a causa disso.

Pode ser que digam que isso é o aquecimento global ou o efeito estufa, prejudicial à nossa saúde.

Todo mundo sente e vê os impactos dos fenômenos estranhos decorrentes da mudança da natureza. Em alguns países vemos terremotos, enchentes, secas, doenças, tsunamis, acidentes, maré alta, vulcões, chuvas com raios e trovoadas. Tudo isso é conseqüência causada pelas mãos dos homens. Eles estão desequilibrando o equilíbrio do ecossistema. Estão colocando em risco a vida da humanidade. O planeta todo vai ao caos.

Alguns estudiosos, como astrônomos, físicos, meteorologistas, que entendem de ciências naturais, podem explicar melhor cientificamente, tecnicamente e filosoficamente. A natureza tem uma lei. Ela age e faz acontecer tudo naturalmente, sem que o homem interfira.

Mas essa lei não é obedecida, é desobedecida. Dá pra entender que temos leis (Constituição) para nos punir. Do mesmo modo, a natureza nos pune. Temos capacidade além da natureza, mas nunca vamos entender as suas ações.

A Terra está sofrendo impactos, está sendo tirada a sua cobertura (vegetação), seu teto destruído (camada dê ozônio), alterada a sua fonte de vida (água) e todas as formas de vida. A sua estrutura sólida, que é a base de sustentação das rochas, solos e águas, está sendo destruída com explosão de dinamites. O lençol freático, com a base rompida, poderá abrir frestas e a água potável poderá secar o seu leito. A rocha, após sofrer explosões, elas racham, se quebram, rompem, se afastam uma das outras. Ela não vai estar sólida.

Na superfície da Terra, quando é provocada a estrutura que sustenta a camada externa, com o tremor, a tendência da vida externa é sofrer impacto. Logo se abre a abertura numa determinada camada da terra, causando a erosão, a fratura da base subterrânea. Começa a encontrar um caminho para o fundo da terra, através das enxurradas penetram as águas potáveis, poderá secar a fonte de água doce, com rompimento das camadas de rochas.

Nosso receio é a liberação de gás prejudicial à vida dos seres humanos. O próprio vulcão inativo se ativará. Será um desastre não só para a Amazônia, o mundo todo sofrerá calado. Ao ser liberado o calor dos vapores do vulcão, quando a água penetrar pelo canal aberto até o manto, o calor através de vapores do contato com a água, o ar será aquecido, sendo prejudicial à vida existente no planeta terra.

Será que o mundo vai permitir esse genocídio que está sendo anunciado com a decisão do governo brasileiro de construir grandes hidrelétricas na região amazônica, causando impactos irreversíveis para toda a humanidade? É a vida na Terra que está em perigo e nós estarnos dispostos a continuar lutando, defendendo a nossa floresta e os nossos rios, para o bem de toda a humanidade. E vocês? Vocês estão dispostos a ser solidários nessa luta?
