---
title: Carta dos Munduruku em apoio aos Guerreiros Guarani Kaiowa, Guerreiros Ka’apor e a todos os guerreiros indígenas do país
date: '2019-02-11T19:27:37.000+10:00'
tags:
- Ipereg Ayu Munduruku
- Munduruku
- Carta
capa:
  descricao: Imagens perdidas da vida
  imagem: imagens/dsc_0268.JPG
descricao: |-
  A carta do povo Ka’apor, povo do Maranhão, divulgada no começo do mês que além de solidarizar com a luta dos povos Guarani Kaiowa e Munduruku, convoca todos a uma contínua união em torno da luta contra os saqueadores de direitos de comunidades tradicionais, donos de terras e governantes deste País. Esta carta, foi respondida pelo Movimento Munduruku Ipereg’ayu, leia a carta na íntegra:
galeria:
- imagens/dsc_2523.JPG
- imagens/dsc_0768.JPG
links_relacionados:
- titulo: Teinha
  link: https://www.superlin.org
- titulo: Abrobrinha
  link: https://www.sem-tempero-nem-http.org
pdfs:
- titulo: Livreta
  autoria: Batata
  ano: 2020
  arquivo: imagens/onion-guide-fanzine.pdf
- titulo: Leitura
  arquivo: imagens/imagem.pdf
  ano: 2021
  autoria: Batata
audios:
- titulo: Rádio Azuelar
  arquivo: https://archive.org/details/radio-azuelar
- titulo: Idade Media
  arquivo: https://archive.org/details/idademedia

---

Carta dos Munduruku em apoio aos Guerreiros Guarani Kaiowa, Guerreiros Ka’apor e a todos os guerreiros indígenas do país.

Nós, Munduruku do médio tapajós, com quatro aldeias localizadas no município de Itaituba-PA e um novo território em processo de autodemarcação dentro da área de empreendimento do governo, onde o território é dos munduruku antes mesmo da chegada dos pariwat invasores do século XV, no tempo dos colonizadores.

Toda a população indígena do Brasil sabe que o governo brasileiro nunca respeitou o nosso direito, mesmo ele existindo na Constituição Federal de 1988. E nenhum político que ocupa o cargo no congresso defende o direito dos povos indígenas. Se fosse um bom politico, não votaria em aprovar a Lei que acaba com os direitos dos povos. Revogaria a PEC 215, a Portaria 303 da AGU e outros projetos de lei, como o novo código da mineração.

Além do mais, os grandes projetos do governo estão atropelando os direitos de todos os povos indígenas do Brasil. Um deles é a construção de Usinas Hidrelétricas no Pará: Belo Monte, São Luiz do Tapajós e mais 4 ao longo do leito do tapajós; uma no Jatobá; uma no Chacorão; outra já em fase final no rio Teles Pires e com continuidade em São Benedito, no rio São Manoel e mais três a serem construídas no rio Jamanxim. E todas elas produzirão energia, mas não beneficiarão nenhuma cidade mais próxima e muito menos a comunidade indígena.

A Energia virá apenas para favorecer as grandes empresas, como as mineradoras e as multinacionais. A hidrelétrica não gerará energia para as pequenas populações que não tem condição de pagar energia cara. Então, com a barragem construída virão mais outros grandes projetos de destruição: a ferrovia; a hidrovia no rio tapajós para escoar os grãos de soja, para exportar ao exterior. E com isso pretendem construir 7 portos no leito do tapajós e asfaltar a BR- 163.

Parentes Guarani Kaiowa, Ka’apor e todos os outros povos que lutam como nós: nós, munduruku, sentimos muitas dores por vocês, pelo tamanho crime que os governantes vêm cometendo, com nossos assassinatos recorrentes. Há séculos os pariwat vêm tomando as nossas terras, vem tirando a vida de nossa floresta que nos dá alimentos para nossa família e que nos dá até medicação. Violentam e estupram a nossa mãe Terra e a deixa desonrada, não a respeita.

O governo, com o seu projeto, não traz “progresso e nem desenvolvimento”, só traz morte. E a população indígena não tem direito de contestar esse tipo de violação. E quando nos manifestamos indignados, com toda razão e com direitos, o governo diz: “estão atrapalhando”. Nós, indígenas, não estamos atrapalhando ninguém. Porque não somos nós que estamos indo a Brasília para tomar as terras dos pariwat e matar. Nem vamos lá para desrespeitar os seus direitos e não invadimos os seus territórios. Como dizem que estamos atrapalhando se foram eles mesmos que fizeram essa tal de Lei para ser obedecida e cumprida e não estão nem respeitando o que eles mesmos escreveram? E não fomos nós. Nós exigimos que o governo garantisse o nosso direito constitucionalmente, na carta magna, na Assembleia constituinte.

Parentes, vamos lutar juntos. É só observar como a natureza nos ensina. Observamos que as formigas taoca nunca caçam sozinhas, mas em bando. Elas entram nas ocas e fazem fugir as mais temíveis cobras, escorpião, centopeia, aranhas, a onça, a grande cobra. Entram em oco de paus e capturam e destroem qualquer espécie que encontram pela frente. Essas formigas são perigosas.

Da mesma forma agem os maribondos. Eles nunca atacam sozinhos. E também as formigas vermelhas ferozes: primeiramente ela vem sozinha e logo em seguida vem o bando para atacar. Os porcos do mato nos ensinam tudo sobre a arte de lutar ou da guerra. As onças, no período do cio, juntam-se em bando para acasalar. As espécies animais nos ensinam tudo isso. Em todos os momentos de nossa vida, nós indígenas, devemos sempre estar juntos.

O momento é esse para lutarmos juntos, contra o nosso maior inimigo, que é o governo. Vamos formar uma grande aliança como o nosso saber nos ensina: a sabedoria do jabuti. Ele é lento, mas não é lerdo. Ele anda devagar, mas não fica para trás. Tem uma resistência e ninguém o derrota. Ele sempre vence. É muito inteligente e sábio.

A única forma é essa: Nós temos que unir nossas forças. Todos os povos indígenas do Brasil e do mundo, desde o norte até o sul, do o oriente ao ocidente. Vamos dar o grito de “basta”! Chega de nos massacrarem, de violarem nossos direitos. Chega de tomarem as nossas terras.

Então, se fizermos uma grande mobilização de nível nacional e internacional poderemos vencer o nosso maior inimigo. Nós não vamos levantar a nossa machadinha para derramar sangue. Queremos mostrar que somos um povo que luta pela vida de todos os seres humanos que dependem da natureza, e não da guerra.

Todos os povos devem se juntar para essa grande batalha pela PAZ, o amor pela natureza, o amor a vida. De todos os seres existentes, que possuem formas de vidas diferentes. Por que nós dependemos de todos eles.

Sawe!

Movimento Ipereg’ayu e Associação Indígena Pariri

Aldeia Sawre Muybu, 15 de julho de 2015
