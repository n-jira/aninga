---
title: Essa é a razão da nossa luta por território
date: '2019-02-11T19:27:37.000+10:00'
tags:
- Ipereg Ayu Munduruku
- Munduruku
- Carta
capa:
  descricao: Imagens perdidas da vida
  imagem: imagens/dsc_0268.JPG
descricao: Carta de Jairo Saw Munduruku comentando a decisão da justiça Federal em obrigar a FUNAI a dar prosseguimento ao reconhecimento da Terra Indígena Sawre Muybu e sobre a importância do território para os Munduruku.
galeria:
- imagens/dsc_2523.JPG
- imagens/dsc_0768.JPG
links_relacionados:
pdfs:
- titulo: Livreta
  autoria: Batata
  ano: 2020
  arquivo: imagens/onion-guide-fanzine.pdf
- titulo: Leitura
  arquivo: imagens/imagem.pdf
  ano: 2021
  autoria: Batata
audios:
- titulo: Rádio Azuelar
  arquivo: https://archive.org/details/radio-azuelar
- titulo: Idade Media
  arquivo: https://archive.org/details/idademedia

---
Essa é a razão da nossa luta por território

Por Jairo Saw Munduruku

É dever de o governo proteger os povos indígenas em todos os aspectos culturais circunstanciais. Garantida o direito à terra tradicionalmente ocupada, com usufruto exclusivamente do povo para a sobrevivência da sua espécie. O governo não deve nos proteger usando a força de opressão, intimidando-nos com uso da violência, sem ambição e sem interesse econômico da nossa terra. Nós não negociamos a terra, trata-se de preservar o que ela nos oferece. Sendo ela bem cuidada ela também nos cuidará.

Todos os seres humanos não somente os indígenas precisam da terra como principal fonte da sobrevivência. Os animais, insetos, árvores, águas e todas as formas existente de vida biológica que nela há, são importante para os indígenas. As árvores precisam de aves, animais e de indigenas para equilibrar a evolução da cadeia alimentar.

Todos os bens comuns que há na terra nós não enxergamos como riqueza. Para possuirmos grandes riquezas não precisamos destruir o patrimônio que nossos antepassados nos deram. Ninguém pode destruir os seus próprios bens patrimoniais e muito menos há dos outros. Nós apenas mantemos como ela sempre deve ficar. Nós a protegemos por que ela é parte de nós. Ela é vida. É delas que comemos frutos tão nutritivos. Quaisquer plantas que, seja ela grandes ou pequenas elas tem as essências naturais para uso medicinais. Por essa razão nunca pensamos em destruir a propriedade que temos. Por que é útil pra nós, para animais, pássaros, insetos, pra peixes e também para os seres humanos que dependem dela. Por que dependemos uns dos outros. É assim que funciona o ecossistema.

Da mesma forma a utilidade da água. E ninguém no mundo em que vivemos sobrevive sem a água, nem as pessoas, nem as plantas e nem mesmo os pequenos insetos. As plantas e insetos se alimentam dos orvalhos que caem a noite. Nós indígenas utilizamos para tratamentos medicinais. Tudo isso é de suma importância pra nós. O que não queremos é que haja mudança da vida do rio. Fazendo a mudança vai comprometer a vida de outros pequenos igarapés que são partes desse afluente. Os animais que frequentam os leitos dos igarapés e que comem dos frutos que se encontram ao longo do curso desse igarapé vão sentir falta. Vão perceber a mudança e sofrerão impactos do modo de seu viver. Os animais que são irracionais sentem os impactos e nós que somos animais racionais, pensamos mais do que eles.

O direito que temos garantido na constituição não é respeitado. O cumprimento não existe pelos próprios que constituíram a Lei na Constituição de 1988. Os países do primeiro mundo, do outro lado do mar não sabem da nossa situação que estamos passando aqui nesse país que se chama Brasil. As informações ao nosso respeito não é levado aos países estrangeiros eles acham que o Brasil está cuidando muito bem a população indígena. Há um silencio nesse meio de comunicação. E ninguém nos conhece por causa da mídia que não comunica através desse meio. O importante pra eles é que as noticias desse genocídio não cheguem ao conhecimento deles.

Se estivermos dizendo “não” aos projetos do governo que pra nós não é viável. Dizem que estamos atrapalhando o desenvolvimento do progresso. Destruir patrimônio de um povo, e todo o seu conhecimento, seu modo de viver, destruindo suas terras e matando a todos isso não é desenvolvimento e nem progresso. É interesse dos grandes latifundiários (agronegócio) que não gostam dos povos indígenas. Só para desenvolver suas experiências de agrotóxicos nas plantas e depois o povo que são pariwat (não-indígenas) consomem e eles nem sabem que estão se alimentando com produtos que contém venenos.

Essa é a razão da nossa luta por território. Todas coisas que existem no meio ambiente ele é considerado sagrado. Não podemos desrespeitar devemos deixar como ela sempre ficou. Por que serve pra nós como para os não índios mas, eles não levam em consideração o que alertamos e o que estamos dizendo. Isso não é fábula, nem lenda isso é pura realidade.

Estamos pedindo a solidariedade da sociedade que despertem da real situação que está acontecendo com o governo que não está dando atenção à população indígena. O direito de todos os brasileiros estão sendo violados. Não é só dos Munduruku. Veja a violação de vários outros direitos: a dos professores, dos pequenos produtores rurais, da saúde, dos consumidores, direito da criança e do adolescente, e etc…!

Avance, lute e não desista!!!

O nosso Direito está em jogo!

Vamos fazer valer o reconhecimento de nossos Direitos!
