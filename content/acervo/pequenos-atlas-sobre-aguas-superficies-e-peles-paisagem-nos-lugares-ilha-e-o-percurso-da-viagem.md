+++
audios = []
descricao = "### PEQUENOS ATLAS SOBRE ÁGUAS, SUPERFÍCIES E PELES: PAISAGEM NOS LUGARES-ILHA E O PERCURSO DA VIAGEM\n\n**CLÁUDIA LEÃO** \n\nFaculdade de Artes Visuais \n\nUniversidade Federal do Pará \n\n**Resumo** \n\nEste trabalho é constituído de uma série de tentativas de pensar a natureza da paisagem fluvial no ambiente da cultura atravessando o campo da arte. Nesse aspecto trago no percurso alguns apontamentos, ou modos de incursão na paisagem, realizados durante as viagens de barco a caminho do que chamei de lugares-ilha, que são ambientes da experiência, ambientes onde ocorrem as tentativas de entrelaçamento entre pele-cultura, pele\u0002ambiente, pele-paisagem, no corpo-pele, criando modo de incorporação, que, para Aby Warburg seria a pathos formal, enlaçamentos que são feitos de encontros e desencontros, de apropriação, de construção de sentido no corpo-pele. Tetsuro Watsuji, expõe uma antropologia de uma paisagem corpórea em que a nossa relação com ambiente parte fundamental da constituição da paisagem. \n\n**Palavras-chave:** pele, corpo, paisagem; entrelaçamento; lugares-ilha, Amazônia. \n\n**Resumen** \n\nEste trabajo es constituído de una serie de intentos de pensar la naturaliza del paisage fluvial en el ambiente de la cultura atravessando el campo del arte. En esa perspectiva, traigo en el percurso algunos puntos o maneras de incursión en el paisage, realizada durante los viages en el barco en dirección al que llamé de lugares-isla, que són ambientes de la experiência, ambientes donde ocurren los intentos de enlazamientos entre la piél-cultura, piél-paisage en el cuerpo-piél, creando modos de incorporación, que, para Aby Warburg se llama la pathos formal, y que son hechos de encuentros y desencuentros, de apropiaciones, de cosntrucciones de sentidos en el cuerpo-piél. Testuro Watsuji expone una antropologia de un paisage-corpóreo en el que nuestra relación cone el ambiente, que es una parte fundamental de la constitución del paisage. \n\n**Palabras-Clave:** piél, cuerpo, paisage; enlazamientos; lageres-isla, Amazónia"
galeria = []
links_relacionados = []
tags = ["LABampe", "Artigo Científico"]
title = "PEQUENOS ATLAS SOBRE ÁGUAS, SUPERFÍCIES E PELES: PAISAGEM  NOS LUGARES-ILHA E O PERCURSO DA VIAGEM"
[capa]
descricao = ""
imagem = ""
[[pdfs]]
ano = 2015
arquivo = "imagens/pequenos-atlas_artigo_claudia-leao.pdf"
autoria = "Claudia Leão"
titulo = "PEQUENOS ATLAS SOBRE ÁGUAS, SUPERFÍCIES E PELES: PAISAGEM  NOS LUGARES-ILHA E O PERCURSO DA VIAGEM"

+++
