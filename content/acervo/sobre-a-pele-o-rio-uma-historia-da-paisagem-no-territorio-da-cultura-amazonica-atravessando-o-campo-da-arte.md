+++
audios = []
descricao = "### SOBRE A PELE, O RIO: UMA HISTORIA DA PAISAGEM NO TERRITÓRIO DA CULTURA AMAZÔNICA ATRAVESSANDO O CAMPO DA ARTE \n\n**Lucas Gouvêa Mariano de Sousa** \n\n**Cláudia Leão**\n\nFaculdade de Artes Visuais/Universidade Federal do Pará - UFPA \n\n**RESUMO:** \n\nPesquisa que pretende pensar a paisagem a partir de uma visão antropológica e cultural, através de uma analise historiográfica do olhar voltado a natureza dentro de um ponto de vista amazônico, na procura de estéticas que estabeleçam um campo de intermediação entre o sujeito e esse ambiente, da natureza-humana e seus afectos geografados através da arte, em um metáfora signica do enfrentamento do corpo com a paisagem amazônica: agua, pele, olho, rio, lagrima, chuva, sangue... é sobre enxergar como queremos ser vistos, e como o estrangeiro enxergam-nos, sobre descolonização desse olhar. Através de um inquietemento pelos transbordes dos rios e da floresta, por suas diluições visuais, pela intensidade no seu discurso na arte e por sua reverberação nas relações culturais evidenciadas no panorama internacional. \n\n**PALAVRAS-CHAVE:** Paisagem, Amazônia, Descolonização \n\n**ABSTRACT:** A research that want to think about the landscape from an anthropological and cultural vision through a historiographical analysis of gaze turned to nature within a point of view Amazon, in seeking to establish an aesthetic field of intermediation between the subject and this environment, human-nature and its created allocated through art, in a semiotic metaphor of the body cope with the Amazonian landscape: water, skin, eye, river, tear, rain, blood is on ... see how we want to be seen, and how foreign they see us on decolonization that look. Through a inquietly transbordes by rivers and forest for their visual dilutions, the intensity in his speech in the art and its reverberation in cultural relations evidenced in the international arena. \n\n**KEYWORDS:** Landscape, Amazon, Decolonization"
galeria = []
links_relacionados = []
tags = ["LABampe ", "Artigo científico"]
title = "SOBRE A PELE, O RIO: UMA HISTORIA DA PAISAGEM NO TERRITÓRIO DA CULTURA AMAZÔNICA ATRAVESSANDO O CAMPO DA ARTE"
[capa]
descricao = ""
imagem = ""
[[pdfs]]
ano = 2014
arquivo = "imagens/lucas_anpap_sobre-o-rio-a-pele.pdf"
autoria = "Lucas Gouvêa Mariano de Sousa, Cláudia Leão"
titulo = "SOBRE A PELE, O RIO: UMA HISTORIA DA PAISAGEM NO TERRITÓRIO DA CULTURA AMAZÔNICA ATRAVESSANDO O CAMPO DA ARTE"

+++
