---
type: sobre
title: O que é o aninga.org

---
A aninga é uma planta amazônica aquática, fluida com os rios, ela pode criar ilhas e torna mais resistentes as beiradas, perenemente expostas à natural erosão que causam a incidência dos fluxos constantes, assim como podem transitar na correnteza dos rios amazônicos.

Tem um caule comprido, as folhas, que ficam na ponta às raízes, são em formato de coração com as ramificações atrás que se assemelham à espinha de um peixe, dá uma flor branca, grande, que se destaca no verde forte das folhas. O seu comprimento transpassa universos diferentes, o fundo lodoso do rio, o meio e a superfície da água, e o ar de fora, cria uma linha, uma ponte, entre eles. É prosaica e bonita como é prosaica e bonita a vida que corre com a água.

A aninga é uma planta que faz da Amazônia (essa palavra estrangeira, nome de registro, e hoje nosso) internamente reconhecível, quem mora na beira do rio sabe o que é, sabe o que faz e para que serve, que bicho que come e os que nela gostam de ficar.

Suas sementes são carregadas pelas águas e nascem em muitas margens, como nascem as casas palafitas de famílias ribeirinhas perto da água, imersas no silêncio denso que carrega o corpo milenar dos rios.

Partindo das experiências vividas nas regiões da Transamazônica e Xingu e na região do/no rio Tapajós, desde 2011, no período da implantação da UHE de Belo Monte, momento de intensa movimentação política no Estado do Pará, a Rede Aninga propõe o criação e manutenção de plataformas digitais que organiza, gerencia e disponibiliza acervos de conteúdos artevistas, políticos, e informativos da região amazônica, notadamente focada em produções que se sustentam em modelos irreverentes, autonomistas, por meio da cartografia social e de falas de grupos e/ou indivídu_e_s, atentando para o armazenamento e cuidado que este tipo de produção requer e que geralmente encontram-se fora do eixo institucional das artes, da cultura e a política. A finalidade é tornar acessível, visível e audível, este tipo de produção, contextualizá-lo, para assim, disponibilizá-lo para consulta pública.

A Rede Aninga também foi pensada como uma forma de contribuir para facilitar o fluxo de informações, e ações artístico-culturais na região amazônica paraense, em uma perspectiva na prática da ética e do cuidado, assim como, também pensar e articular ações de estratégia para segurança e prevenção das diversas violações aos direitos garantidos nas convenções internacionais e na constituição federal, por meio da sua divulgação, circulação e disponibilização de estudos realizados sobre, com pelos povos e comunidades tradicionais amazônidas, levando em consideração a dificuldade de conectividade à distância de tais comunidades, a sua vulnerabilidade e a amplitude da região amazônica.

A constituição da rede Aninga trabalha em parceria e/ou aliada a agenciodoras/agenciadores individuais e coletivos, artísticos, associações e grupos políticos de povos e comunidades tradicionais amazônicos, como: Movimento Ipereg Ayu, Associação Wakoborun de Mulheres Munduruku, Grupo de Pesquisa Lab Ampe articulado ao projeto de Sobre Outras História Crítica da Fotografia e Arte nas/das Amazônias Paraenses e A Projeto Nova Cartografia Social da Amazônia, Movimento Xingu Vivo Para Sempre, Rádio Áudios-Furos de Rios e Rádio Catimbó, Núcleos de Articulação e Resistência do Quilombo Campina/Vila União e Vila do Tauá; Sala Táta Kinamboji de Ensino Arte e Cultura Afro-amazônica, Grupo de Estudos Antiracistas e Antisexistas Zélia Amador de Deus, Amazônia em Chamas, Qualquer Quoletivo, e Terreiro Casa de Umbanda de Mãe Herondina.

  
Rede de suporte/coletivos

em ações articuladas o aninga.org trabalha em redes como mediador tanto na formação de educação digital assim, quanto em favor de lutas ser o mediador entre grupos coletivos e agenciadores e agenciadoras. Partindo de uma relaçnao ética, e de cuidado com os conteúdos de interesse de povos e comunidades tradicionais e da possibilida de auto gerenciamento destas informção este lugar deve guardar e tornar acessivel e segura as infromações correntes.

> SEMINÁRIO E LANÇAMENTO
>
>   
> **A rede Aninga convida para o Seminário de Virtual de Lançamento do Acervo Digital Aninga**
>
> 27/09: seminário 19h - 21h
>
> 28/09: Lançamento 19:30 -20:30
>
> Transmissão aberta para a comunidade na cidade de Altamira
>
> LOCAL da transmissão: bairro Laranjeiras - Travessa Pirapitinga (rua do Igarapé), 434.
>
>   
> Evento com certificado
>
> não se esqueça de trazer sua máscara!

A Rede Aninga propõe o desenvolvimento de uma plataforma digital que organiza, gerencia e disponibiliza acervos de conteúdos artísticos políticos da região amazônica, notadamente focada em produções que se sustentam em modelos irreverentes autonomistas, e geralmente encontra-se fora do eixo institucional das artes e cultura. A rede visiona a produção de bibliotecas digitais dentro desta plataforma, com conteúdos selecionados a partir da cartografia social de grupos e/ou indivíduos, atentando para o armazenamento e cuidado que este tipo de produção requer. A finalidade é tornar visível este tipo de produção, contextualizá-lo, para assim disponibilizá-lo para consulta pública.

Entendemos a cartografia como uma ferramenta metodológica que nos permite localizar o tempo histórico(cronologia que fez sentido para nós e para nossas histórias) ressaltando o contexto político e aspectos culturais que permeiam estas produções. O processo de organização desses acervos a partir de métodos cartográficos também nos possibilitam pensar coletivamente, considerando a subjetividade destas produções …

  
![](imagens/logos-edital-de-artes-visuais_p-b.png)